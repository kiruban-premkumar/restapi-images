require 'sinatra'
require 'mongoid'
require 'aws-sdk'
require './image'

configure do
  Mongoid.load!("config/mongoid.yml", settings.environment)
  set :server, :puma
end

# list all images
get '/images' do
  Image.all.to_json
end

# upload an image
post '/images' do

  name = params[:name]
  description = params[:description]
  file = params['file']

  if name.nil? or name.empty?
    halt 400, {:message=>"name field cannot be empty"}.to_json
  end

  Aws.config.update(
    region: 'us-east-1',
    endpoint: "http://minio:9000",
    force_path_style: true,
    credentials: Aws::Credentials.new(
      "AKIAIOSFODNN7EXAMPLE",
      "wJalrXUtnFEMI/K7MDENG/bPxRfiCYEXAMPLEKEY"
    )
  )
  s3 = Aws::S3::Resource.new(Aws::S3::Client.new)
  key = File.basename file['filename']
  obj = s3.bucket("scaleway-images-storage").object(file['filename'])
  obj.upload_file(file['tempfile'].open)

  image = Image.new(:name => name, :description => description, :public_url => obj.public_url)

  if image.save
    image.link = "/images/"+image.id
    image.save
    status 201
  else
    [500, {:message=>"Failed to save Image"}.to_json]
  end
end

# view one image
get '/images/:id' do
  image = Image.find(params[:id])
  return status 404 if image.nil?
  redirect image.public_url, 301
end

# delete an image
delete '/images/:id' do
  image = Image.find(params[:id])
  return status 404 if image.nil?
  image.delete
  status 202
end