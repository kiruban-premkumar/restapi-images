class Image
  include Mongoid::Document
  include Mongoid::Timestamps
  
  field :name, type: String
  field :description, type: String
  field :public_url, type: String
  field :link, type: String
end