# Testing the API

## Get all images

```
kiruban@iMac-de-Kiruban ~/Code/restapi (master*) $ curl -X GET http://localhost:4567/images --silent | jq .
[
  {
    "_id": {
      "$oid": "5a36d89062c74000017d1480"
    },
    "created_at": "2017-12-17T20:50:24.288+00:00",
    "description": "cloudservice1",
    "link": "/images/5a36d89062c74000017d1480",
    "name": "scaleway1",
    "public_url": "http://minio:9000/scaleway-images-storage/scaleway1.png",
    "updated_at": "2017-12-17T20:50:24.290+00:00"
  },
  {
    "_id": {
      "$oid": "5a36d89062c74000017d1481"
    },
    "created_at": "2017-12-17T20:50:24.332+00:00",
    "description": "cloudservice2",
    "link": "/images/5a36d89062c74000017d1481",
    "name": "scaleway2",
    "public_url": "http://minio:9000/scaleway-images-storage/scaleway2.png",
    "updated_at": "2017-12-17T20:50:24.334+00:00"
  },
  {
    "_id": {
      "$oid": "5a36d89062c74000017d1482"
    },
    "created_at": "2017-12-17T20:50:24.379+00:00",
    "description": "cloudservice3",
    "link": "/images/5a36d89062c74000017d1482",
    "name": "scaleway3",
    "public_url": "http://minio:9000/scaleway-images-storage/scaleway3.png",
    "updated_at": "2017-12-17T20:50:24.381+00:00"
  },
  {
    "_id": {
      "$oid": "5a36d89062c74000017d1483"
    },
    "created_at": "2017-12-17T20:50:24.427+00:00",
    "description": "cloudservice4",
    "link": "/images/5a36d89062c74000017d1483",
    "name": "scaleway4",
    "public_url": "http://minio:9000/scaleway-images-storage/scaleway4.png",
    "updated_at": "2017-12-17T20:50:24.429+00:00"
  }
]
```

## Upload somes images

```
curl -X POST http://localhost:4567/images -F "file=@scaleway1.png" -F "description=cloudservice1" -F "name=scaleway1"
curl -X POST http://localhost:4567/images -F "file=@scaleway2.png" -F "description=cloudservice2" -F "name=scaleway2"
curl -X POST http://localhost:4567/images -F "file=@scaleway3.png" -F "description=cloudservice3" -F "name=scaleway3"
curl -X POST http://localhost:4567/images -F "file=@scaleway4.png" -F "description=cloudservice4" -F "name=scaleway4"
```

## Get an image

```
kiruban@iMac-de-Kiruban ~/Code/restapi (master*) $ curl -I -X GET http://localhost:4567/images/5a36d89062c74000017d1483
HTTP/1.1 301 Moved Permanently
Content-Type: text/html;charset=utf-8
Location: http://minio:9000/scaleway-images-storage/scaleway4.png
X-XSS-Protection: 1; mode=block
X-Content-Type-Options: nosniff
X-Frame-Options: SAMEORIGIN
Content-Length: 0
```

## Delete an image

```
curl -X DELETE http://localhost:4567/images/5a368e65fda2e717979491f8
```


# Running API, MINIO & creating buckets


```
# Run Minio, create a default bucket, run api & mongo

docker-compose up
```